
### Compilation in Docker

```
docker run -it ubuntu:rolling /bin/bash

apt update

apt install -y git

git clone https://invent.kde.org/kde/krita.git
cd krita

sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list

export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

apt update

apt build-dep -y krita 

mkdir build
cd build

cmake ../  -DCMAKE_BUILD_TYPE=Debug -DKRITA_DEVS=ON

make -j8

```

### Gitlab Docker 

```
####################
    - sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list

    - export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

    - apt update

    - apt build-dep -y krita 

    - mkdir build
    - cd build
    - cmake ../  -DCMAKE_BUILD_TYPE=Debug -DKRITA_DEVS=ON
####################

----- make -j8

```
